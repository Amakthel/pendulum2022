function pkv=getoscpks(x,y,pkh)
[mkpf,locspf]=findpeaks(y,x,'MinPeakProminence',pkh);
[mkp,locsp]=findpeaks(y,x,'MinPeakDistance',0.9*mean(diff(locspf)));
figure,plot(x,y);
title(['Peaks with minimum height',num2str(pkh)]);
hold on,plot(locsp,mkp,'ro');
pkv(:,1)=reshape(locsp,[length(locsp),1]);
pkv(:,2)=reshape(mkp,[length(mkp),1]);
hold off
end


