function [t,theta]=ProcessVideom(filename)
offset = 60;
vid= VideoReader([filename,'.AVI']);
numFrames=min(vid.NumberofFrames,30*45);
%imarray=read(vid,[1 numFrames]);
fram2read=offset:numFrames;
for k=1 : length(fram2read)
    grayimg(:,:,:,k)=imadjust(read(vid,fram2read(k)),[0.76 0.7 0.7;1 1 1],[]);
end
mimg=max(grayimg,[],4);
imshow(mimg,[])

%Draw a region to include only the top light
title('Draw a rectangle to include only the trace of the top light')
disp('Draw a rectangle to include only the trace of the top light')
%up_rect=round(getrect);
bwu=roipoly(mimg);

%Draw a region to include only the lower light
title('Draw a rectangle to include only the trace of the lower light')
disp('Draw a rectangle to include only the trace of the lower light       \n')
%low_rect=round(getrect);
bwl=roipoly(mimg);

%subimgu=grayimg(up_rect(2):up_rect(2)+up_rect(4), up_rect(1):up_rect(1)+up_rect(3),:);
%subimgl=grayimg(low_rect(2):low_rect(2)+low_rect(4), low_rect(1):low_rect(1)+low_rect(3),:);

for k=1 : numFrames
   fprintf( '\b\b\b\b\b\b\b\b\b\b\b%05i/%05i', k+offset,  numFrames  );
   gimg=rgb2gray(read(vid,k+offset)); 
%    subimgu=gimg(up_rect(2):up_rect(2)+up_rect(4), up_rect(1):up_rect(1)+up_rect(3));
%    subimgl=gimg(low_rect(2):low_rect(2)+low_rect(4), low_rect(1):low_rect(1)+low_rect(3));
   subimgu=double(gimg).*bwu;
   subimgl=double(gimg).*bwl;
   timg=subimgu;
   bims=(timg>(max(timg(:)-15)));
   s=regionprops(bims,'centroid');
   posv=[];

   for i=1:length(s)
    posv=[posv;s(i).Centroid(1) s(i).Centroid(2)];
   end
   posva=sortrows(posv,2);
   x_up(k)= posva(1,1);
   y_up(k)= posva(1,2);
   timg=subimgl;
   bims=(timg>(max(timg(:)-20)));
   s=regionprops(bims,'centroid');
   posv=[];
   for i=1:length(s)
   posv=[posv;s(i).Centroid(1) s(i).Centroid(2)];
   end
   posva=sortrows(posv,-2);
   x_low(k)=posva(1,1);
   y_low(k)=posva(1,2);

end
% 
% x_up=x_up+up_rect(1)-1;
% y_up=y_up+up_rect(2)-1;
% x_low=x_low+low_rect(1)-1;
% y_low=y_low+low_rect(2)-1;
theta=atan2(y_low-y_up, x_low-x_up);
t=(0:length(theta)-1)/vid.FrameRate;
close all;
figure,plot(t,theta,'-o')
save([filename,'.mat'],'t','theta')