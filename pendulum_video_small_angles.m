function pendulum_video_small_angles
%video analysis for physical pendulum
%this program will process a movie of the pendulum and determine the amplitude, damping constant, phase,
%and angular velocity of the oscillation.
%The user inputs include the AVI_name, the range and stepsize of frames to
%analyze. Also need to set the ROI, which here is the 
%center, radii which contain the paths of the lights and others.
%Input paragraphs are delimited using %************************
% It saves
% 1) the figure showing the oscillation and its fitting as AVI_name.fig
% 2) the oscillation and fitting parameters as as AVI_name.mat

%note if the movie has been analyzed, and if a .mat file exists, then the
%program will skip loading in the movie and go straight to the analysis
%section

%inputs are here
%************************
AVI_name = 'double6';
%***********************
if ~exist( strcat( AVI_name, '.mat' ) )

% Process the video without ploting the dots on images. You will be asked
% to specify the starting frame, interval and ending frame. if you want to
% process the whole video without 
  [t, theta]=ProcessVideom(AVI_name);
else
    load( strcat( AVI_name, '.mat' ) );

end
    clf;
    plot( t, theta, '-.' );
    ylabel( '\theta' );
    xlabel( 't (sec)' );
 title('Click on the point where the first maximal angle is located')  
 [ts,thets]=ginput(1);
 tstart=ts;
 %tend=ts(2);
 %ids=find(t>tstart & t < tend);
 
 ids=find(t>tstart);
 tcut=t(ids);
 thetacut=theta(ids);
 [ym,idst]=max(thetacut);

 tfit=tcut(idst:end)-tcut(idst);
 thetafit=thetacut(idst:end);
 toffset=tcut(idst);   
%%
%general function for damped pendulum in the small angle limit, where the
%period is (almost) constant
fitresl=fitdecayosc(tfit,thetafit);
theta0=fitresl.amp;
omega0=sqrt((2*pi*fitresl.freq)^2+1/(fitresl.decayt)^2);
offset=fitresl.vshift;
phi=fitresl.phs;
b=1/fitresl.decayt*2/omega0;
figure(1)
savefig(strcat( AVI_name, '.fig'))

save( strcat( AVI_name, '.mat' ),'toffset', 'tfit','thetafit','b','omega0','offset','phi','theta0','fitresl','-append' );
%%
return;

