function out=decayosc(x,amp,decayt,freq,phs,vshift)
out=amp*exp(-x/decayt).*cos(2*pi*freq*x+phs)+vshift;
end