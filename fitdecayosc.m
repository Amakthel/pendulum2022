function outp=fitdecayosc(xin,yin)
%% Fit the oscillation data to a damped oscillation 
%% y=amp*exp(-x/decayt).*cos(2*pi*freq*x+phs)+vshift
%%  With vshift representing the mean value of y. 
%%

%make the first x value to be zero

x=xin(:)-xin(1);
%center the data;
y=yin(:)-mean(yin);

%guess the height of the first peak
%[amp_guess,Ind]=max(abs(y-mean(y))); 

%find the peaks of the oscillation
pks=getoscpks(x,y-mean(y),abs(y(1)/5));
% fac=1;
% if length(pks)<2 || isempty(pks);
%     pkm=getoscpks(x,-(y-mean(y)),amp_guess/2);
%     pks=[pks;pkm];
%     fac=2;
% end
% 
% if size(pks,1)<2 || isempty(pks);
% %    pkm=getoscpks(x,-(y-mean(y)),amp_guess/2);
% %    pks=[pks;pkm];
%     clear pks;
%     fac=2;
% 	
% 	h=msgbox({'Please pick peaks manually.'},'Auto peak detection failed','modal');
%     uiwait(h)
%     plot(x,y)
%     title({'Left click on the locations of peaks and valleys.','Press ENTER when you are done.'},'FontSize',20)
% 	pks=ginput;
% end


% Find the decay constant by fitting the peak values to an expoential decay
ft = fittype( 'a*exp(-x/decayt)', 'independent', 'x', 'dependent', 'y' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.Lower = [0.5*y(1) 0];
opts.StartPoint = [y(1) 101];
fitamps = fit(pks(:,1), pks(:,2), ft, opts);
decay_guess=fitamps.decayt;
amp_guess=fitamps.a;
outp.amp=fitamps.a;
outp.decayt=fitamps.decayt;
amperrs=confint(fitamps);
decayterrs=amperrs(:,2);
% pks=sortrows(pks);
% p=polyfit(pks(:,1),log(pks(:,2)),1);
%decay_guess=-1/p(1);

%%find frequency
pkintv=diff(pks(:,1));
T.val=abs(mean(pkintv));
T.err=abs(std(pkintv));
outp.freq=1/T.val;
freqerr=[outp.freq*(1-T.err/T.val/2);outp.freq*(1+T.err/T.val/2)];
freq_guess=1/abs(mean(diff(pks(:,1))));

if y(1)<mean(y)
phs_guess=pi;
else
phs_guess=0;
end

%ftk=fittype('decayosc(x,amp,decayt,freq,phs,vshift)');
%ft=fit(x,y,ftk,'StartPoint',[amp_guess,decay_guess,freq_guess, phs_guess, abs(mean(y))],'Lower',[0.5*amp_guess,decay_guess*0.8,0,-pi/8+phs_guess,-5*abs(mean(y))],'Upper',[Inf,decay_guess*3,Inf,pi/8+phs_guess,max([1e-5 5*abs(mean(y))])]);
opts = fitoptions('Method','Nonlinear','Normalize','On');
ftk=fittype('amp*exp(-x/decayt).*cos(2*pi*freq*x+phs)+vshift', 'problem',{'amp','decayt','freq','vshift'},'independent','x','dependent','y','Options',opts);
ft=fit(xin(:),yin(:),ftk,'problem',{amp_guess,decay_guess,freq_guess,mean(yin)},'StartPoint',[phs_guess],'Lower',[-pi/2+phs_guess],'Upper',[pi/2+phs_guess]);
%ft=fit(x,y,ftk,'problem',freq_guess,'StartPoint',[amp_guess,decay_guess, phs_guess, mean(y)],'Lower',[0.8*amp_guess,0.5*decay_guess,-pi/10+phs_guess,-5*abs(mean(y))],'Upper',[Inf,1.5*decay_guess,pi/10+phs_guess,max([1e-5 5*abs(mean(y))])]);
close all;
figure(1)
plot(xin,yin,'bo');
hold on,plot(xin,decayosc(xin,ft.amp,ft.decayt,ft.freq,ft.phs,ft.vshift),'r-','Linewidth',2)
%outp.amp=ft.amp;
outp.phs=ft.phs;
outp.vshift=ft.vshift;
fterrs=confint(ft);
outp.uncertainty=[amperrs freqerr fterrs];
end
