% IPL Data Processing
% April 26, 2022
% Charlie Tribble

%% Read Data

close all; clear variables; clc;

RAW_DATA_10_1 = readtable("10degree1.csv");

startData   = 1;
endData     = 1000;

timestamp   = RAW_DATA_10_1{startData:endData,1};
angle       = RAW_DATA_10_1{startData:endData,2};

timestamp = timestamp - timestamp(1);

%% Plotting

figure;
dampedFit = fittype('a*exp(-b*x)*cos(c*x+d)+f','dependent',...
    {'y'},'independent',{'x'},'coefficients',{'a','b','c','d','f'});

%fplot(@(x) 0.15*exp(-0.005*x)*cos(4*x+pi)+1.55,"LineWidth",1,'Color','#000000'); hold on;

[myFit, gof] = fit(timestamp,angle,dampedFit,'StartPoint',[0.15 0.005 4 pi 1.55]);
disp(myFit); disp(gof);
plot(myFit,timestamp,angle,'.--');